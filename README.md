Install dependencies
======================
npm install react next react-router
npm i create-next-app

Create your next-app
=====================
npx create-next-app

Project structure
=================

Pages Folder -
    Create components and pages

Install dependecies -
    Update the ecommerce/package.json dependecies list

Execute the below command to install mentioned versions of all the dependencies
    npm install --legacy-peer-deps

Run the web app
    npm run dev

Configure Sanity
=================
Allow us to easily manage our data for our entire ecommerce store through out our entire application development

Go to the link: https://www.sanity.io/javascriptmastery2022
npm install -g @sanity/cli
(
If can't install directly due to permission use sudo or root user

For the docker image using in here used below command to change the run time password 
    docker exec -itu 0 {containerid} passwd

)

Initialize sanity coupon
    sanity init

Sanity commands
    sanity docs
    sanity manage

From sanity root folder
    sanity start

Sanity Schema concepts
=======================

* Create your schemas inside schemas folder

* schemas.js file helps to use schemas. Update the schemas.js file with added new product and banner schema. 

* Check on the local sanity and it have now two schemas created for 
    product
    banner

* Create a one instance for product. It can fill details there easily. 

* Now we have our DB schemas and one product instance there. We implemented our entire DB there.

Develop Fron End application
=============================

* index.js inside ecommerce/pages/index.js is the main page of the application
    clean default content
    rface type and it will create a skeleton

------------Skeleton---------------
import React from 'react'

const index = () => {
    return (
        <div>
            index
        </div>
    )
}

export default index
-----------------------------------

Fix babel
    * create .babelrc file
    * Update .eslintrc.json file

-----------------------------------

Rename Index to Home
    * Index -> Home

-----------------------------------

Update styles sheets
======================

Update the content of /styles/globals.css

Component structure
=====================

* Create components folder
* Create components files as in the components folder
    - run rafce short cut or follow the below structure for initial content

        import React from 'react'

        const Product = () => {
            return (
                <div>
                    Product
                </div>
            )
        }

        export default Product

* Create index.js inside components folder and export all components

    export { default as Footer } from './Footer';
    export { default as Layout } from './Layout';
    export { default as Navbar } from './Navbar';
    export { default as Product } from './Product';
    export { default as HeroBanner } from './HeroBanner';
    export { default as FooterBanner } from './FooterBanner';
    export { default as Cart } from './Cart';

* Import all components for the pages/index.js file
    - This is simple because of components/index.js

        import { Product, FooterBanner, HeroBanner } from '../components';

* Install dependecy for preset-react to resolve error

    npm install --save-dev @babel/preset-react
    npm install @babel/core --save

    Update .babelrc

        {
            "presets": ["@babel/preset-react"]
        }

    Update ,eslintrc.json

        {
        "extends": [ "next/babel", "next/core-web-vitals"]
        }

* Import react in pages/_app.js

    import React from 'react';

    import '../styles/globals.css'

    function MyApp({ Component, pageProps }) {
    return <Component {...pageProps} />
    }

    export default MyApp

* Add custom components into the index.js main file 
-------------------------------------------------------

    Herobanner test -> <HeroBanner/>
    Footer test -> <FooterBanner/>

            <>
                <HeroBanner/>
            
                <div className="products-heading">
                    <h2>Best Selling Products</h2>
                    <p>Speakers of many variations</p>
                </div>

                <div className="products-container">
                    {['Product 1', 'Product 2'].map(
                        (product) => product
                    )}
                </div>

                <FooterBanner/>
            </>

* Create components content
=============================

* Update HeroBanner with customized content
--------------------------------------------
import React from 'react'
// import Link from 'next/Link'
import NextLink from 'next/link';

const HeroBanner = () => {
    return (
        <div className="hero-banner-container">
            <div>
                <p className="beats-solo"> SMALL TEXT</p>

                <h3>MID TEXT</h3>
                <img src="" alt="headphones" className="hero-banner-image"/>

                <div>
                    <NextLink href="/product/ID">
                        <button type="button">BUTTON TEXT</button>
                    </NextLink>

                    <div className="desc">
                        <h5>Description</h5>
                        <p>DESCRIPTION</p>
                    </div>
                </div>

            </div>
        </div>
    )
}

export default HeroBanner


Connect application with sanity
================================

* Create lib folder
* Create client.js file
* Update content 
        import sanityClient from '@sanity/client';
        import imageUrlBuilder from '@sanity/image-url';

        export const client = sanityClient({
        projectId: 'r80zsyqv', //sanity knows which project connected with
        dataset: 'production', // sanity knows it is development or production
        apiVersion: '2022-06-05',
        useCdn: true,
        token: process.env.NEXT_PUBLIC_SANITY_TOKEN
        });

        const builder = imageUrlBuilder(client);

        export const urlFor = (source) => builder.image(source);

    Values of projectId, dataset, apiVersion can update using sanity.io project details

    Token generate from sanity and paste in a new file called .env and assigned to a variable (NEXT_PUBLIC_SANITY_TOKEN) and update client.js token value accordingly

        NEXT_PUBLIC_SANITY_TOKEN = <value>

* import sanity from index.js

* Fetch data from sanity client
--------------------------------
1. React JS -
    Create user effect (life cycle method) inside pages/index.js
    Then just call the client inside div contents and fetch the data

2. Next JS -
    * getServerSideProps for next js
        https://nextjs.org/docs/basic-features/data-fetching/get-server-side-props

    * If you export a function called getServerSideProps (Server-Side Rendering) from a page, Next.js will pre-render this page on each request using the data returned by getServerSideProps.

    * Define the funtion in pages/index.js

        export async function getServerSideProps(context) {
            return {
                props: {}, // will be passed to the page component as props
            }
        }
    
    export const getServerSideProps = async () => {
        const query = '*[_type == "product"]';
        const products = await client.fetch(query);

        const bannerQuery = '*[_type == "banner"]';
        const bannerData = await client.fetch(bannerQuery);

        return {
            props: { products, bannerData }
        }
    }

3. From the pages/index.js file, its Home updated to take Dynamic Block as parameters
    const Home = ({ products, bannerData }) => {

    }

4. getServerSideProps gets called on every request and pass return value to Home as properties

5. Then those fetched data can pass as properties to other components too

    From Home to Banner
        pages/index.js

            <HeroBanner heroBanner={bannerData.length && bannerData[0]}  />

        components/HeroBanner.jsx
            const HeroBanner = ({ heroBanner }) => {
            
            }

6. Passed object can use within compoennts to refer variable values which fetched from DB

    <p className="beats-solo">{heroBanner.smallText}</p>

Update Layout strcuture to get a better view
==============================================

* Update components/Layout.jsx
    const Layout = ({ children }) => {
        return (
        <div className="layout">
            <Head>
            <title>Green Store</title>
            </Head>

            <header>
            <Navbar />
            </header>

            <main className="main-container">
            {children}
            </main>

            <footer>
            <Footer />
            </footer>
            
        </div>
        )
    }

* Update pages/_app.jsx

    Current content
    -----------------
        import React from 'react';

        import '../styles/globals.css'

        function MyApp({ Component, pageProps }) {
        return <Component {...pageProps} />
        }

        export default MyApp

    * Above <Component/> means, the component which is currently on. 
    * So, if you are on one of product details page, that is going to be the above Component.
    * If on Home page, then that is going to be that component.

    Updated Content
    -----------------

    Let's wrap the current component, 

        import React from 'react';

        import {Layout } from '../components';
        import '../styles/globals.css'

        function MyApp({ Component, pageProps }) {
        return (
            <Layout>
            <Component {...pageProps} />
            </Layout>
        )
        }

        export default MyApp

    No the content look like this

        Navbar
        EMPTY
        Footer

    How to make _app.js Component tag appear inside the Layout.jsx

    In React, whatever pass inside a component (tags between a tag) (Child tags of a tag), you get access to it through a prop called children

    Now we have space in the banners

* Create view product page - Page witha dynamic route, dynamic page name, dynamic html content, dynamic URLS
=================================================================================================================

* Need to visit numorous products which can't predict beforehand.
    - It can be localhost:3000/product/speaker
    - Or can be localhost:3000/product/headphone

* To visit these URLS, add product folder inside pages

* Create [slug].js file inside pages/product/[slug].js

* Name within square brackets means it is going to be a dynamic page
    - It can be localhost:3000/product/speaker
    - Or can be localhost:3000/product/headphone

    -[slug].js represents all the speaker, headphone, headphone_c dynamic names

* This is the real beauty of File base routing in Next JS.
    - Within didn't have implemented any kind of library like react router
    - We just created a folder with a specific file name
    - We imediately created the .jsx and the logic of the component

* slug js need to know the details of the selected product. How it can know the selected product?

* getStaticProps function can call from the slug js

    - If you export a function called getStaticProps (Static Site Generation) from a page, Next.js will pre-render this page at build time using the props returned by getStaticProps.

* You should use getStaticProps if:

    - The data required to render the page is available at build time ahead of a user’s request
    - The data comes from a headless CMS (this is exact the scenario in here)
    - The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of which can be cached by a CDN for performance
    - The data can be publicly cached (not user-specific). This condition can be bypassed in certain specific situation by using a Middleware to rewrite the path.

Get static props also have a benefit:
    - can give params
    - Outside of the params can get access actual URL query
        { params: { slug }}
    - slug can be 
        headphones
        speakers

    - From the slug variable value, can query to get the current product details

                    export const getStaticProps = async ({ params: { slug }}) => {
                        // current slug query
                        const query = `*[_type == "product" && slug.current == '${slug}'][0]`;

                        // similar products query
                        const productsQuery = '*[_type == "product"]'
                        
                        const product = await client.fetch(query);
                        const products = await client.fetch(productsQuery);
                    
                        console.log(product);
                    
                        return {
                        props: { products, product }
                        }
                    }

    As get server side props, all these values are going to be delivered straight for us right inside the props of ProductDetails


                    const ProductDetails = ({ product, products }) => {

                        // de structure the values from product
                        const { image, name, details, price } = product;


                        return (
                            <div>
                                <div className="product-detail-container">
                                    <div>
                                        <div className="image-container">
                                            <img src={urlFor(image && image[0])} className="product-detail-image" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }

* getStaticPaths function can call from the slug js

    Here still can't render the entire page without dynamic path details. To complete paths in the page dynamically, override the getStaticPaths()

                    export const getStaticPaths = async () => {

                        // sanity language is similar to graph ql
                        const query = `*[_type == "product"] {
                        slug {
                            current
                        }
                        }`; // give me all the products, but return which have current slug property

                        const products = await client.fetch(query);

                        const paths = products.map((product) => ({
                        params: { 
                            slug: product.slug.current
                        }
                        }));
                    
                        return {
                        paths,
                        fallback: 'blocking'
                        }
                    }

getStaticPaths
---------------

* If a page has Dynamic Routes and uses getStaticProps, it needs to define a list of paths to be statically generated.

* When you export a function called getStaticPaths (Static Site Generation) from a page that uses dynamic routes, Next.js will statically pre-render all the paths specified by getStaticPaths.

    - Inside a selected object we have many other links
    - Next JS need to build those links too
    - Have to repeat the same process to inner links too
    - So then the Next JS can prepare all that data and deliver it as quickly as possible
    
When to use 
    You should use getStaticPaths if you’re statically pre-rendering pages that use dynamic routes and:

    The data comes from a headless CMS
    The data comes from a database
    The data comes from the filesystem
    The data can be publicly cached (not user-specific)
    The page must be pre-rendered (for SEO) and be very fast — getStaticProps generates HTML and JSON files, both of which can be cached by a CDN for performance

When does getStaticPaths run
    getStaticPaths will only run during build in production, it will not be called during runtime. 
    
    You can validate code written inside getStaticPaths is removed from the client-side bundle with this tool.

    getStaticProps runs during next build for any paths returned during build
    
    getStaticProps runs in the background when using fallback: true

    getStaticProps is called before initial render when using fallback: blocking


