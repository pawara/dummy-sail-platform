import React from 'react';
// import Link from 'next/Link'
import NextLink from 'next/link';

import { urlFor } from '../lib/client';

/*  * Take one banner per each time. 
    * Display banner data under different sections
    * Refer sanityecommerce/shemas/banner.js to get the defined attributes/properties of the banner (banner schema in DB)
    * Those values can display from the heroBanner object using dot (.) operation
*/
const HeroBanner = ({ heroBanner }) => {
    return (
        <div className="hero-banner-container">
        <div>
          <p className="beats-solo">{heroBanner.smallText}</p>
          <h3>{heroBanner.midText}</h3>
          <h1>{heroBanner.largeText1}</h1>
          
          {/* 
              * src tag need the image URL
              * Pass schema.image to the urlFor to display the image from the image URL
          */}
          <img src={urlFor(heroBanner.image)} alt="headphones" className="hero-banner-image" />
  
          <div>
                {/* 
                  * To shop the current banner items, button will givea short cut
                  * It should lead to the banner related item shopping window
                  * this url will resolve to 
                      http://localhost:3000/product/headphones
                */}
                <NextLink href={`/product/${heroBanner.product}`}>
                  <button type="button">{heroBanner.buttonText}</button>
                </NextLink>

                <div className="desc">
                  <h5>Description</h5>
                  <p>{heroBanner.desc}</p>
                </div>
          </div>
        </div>
      </div>
    )
}

export default HeroBanner
