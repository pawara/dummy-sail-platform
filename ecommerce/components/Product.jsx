import React from 'react';
import NextLink from 'next/link';

import { urlFor } from '../lib/client';

const Product = ({ product: { image, name, slug, price } }) => {
    return (
      <div>
        <NextLink href={`/product/${slug.current}`}>
          <div className="product-card">
            <img 
              src={urlFor(image && image[0])}
              width={250}
              height={250}
              className="product-image"
            />
            <p className="product-name">{name}</p>
            <p className="product-price">${price}</p>
          </div>
        </NextLink>
      </div>
    )
  }

export default Product
