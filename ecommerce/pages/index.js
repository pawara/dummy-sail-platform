import React from 'react'

import { client } from '../lib/client';
import { Product, FooterBanner, HeroBanner } from '../components';

/*   *Here get data from getServerSideProps( ) call. 
        It returns props caontaining both products and bannerData.
    * Home define to call with Dynamic Block as a parameter. 
    * From the upper level, index.js default Home get the call using definedreturn value of the getServerSideProps( )
    * HeroBanner updated to get properties. It passes first property of bannerData
*/
const Home = ({ products, bannerData }) => {
    return (
        <>
            <HeroBanner heroBanner={bannerData.length && bannerData[0]}  />
            {/* {console.log("Hellow")} */}
            {console.log(bannerData)}
        
            <div className="products-heading">
                <h2>Best Selling Products!</h2>
                <p>Speakers of many variations</p>
            </div>
            {/* This iterate through all available products and pass each object into the Product component to rendering */}
            <div className="products-container">
                {products?.map((product) => <Product key={product._id} product={product} />)}
            </div>

            <FooterBanner footerBanner={bannerData && bannerData[0]} />
        </>
    )
}

// This gets called on every request
export const getServerSideProps = async () => {
    const query = '*[_type == "product"]'; // this is the defined sanity query
    const products = await client.fetch(query);
  
    const bannerQuery = '*[_type == "banner"]';
    const bannerData = await client.fetch(bannerQuery);
  
    return {
      props: { products, bannerData }
    }
}

export default Home

