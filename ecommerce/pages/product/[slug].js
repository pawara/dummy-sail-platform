import React from 'react'

import { client, urlFor } from '../../lib/client';

const ProductDetails = ({ product, products }) => {

    // de structure the values from product
    const { image, name, details, price } = product;


    return (
        <div>
            <div className="product-detail-container">
                <div>
                    <div className="image-container">
                        <img src={urlFor(image && image[0])} className="product-detail-image" />
                    </div>
                </div>
            </div>
        </div>
    )
}


/*
    * Inside a selected object we have many other links
    * Next JS need to build those links too
    * Have to repeat the same process to inner links too
    * So then the Next JS can prepare all that data and deliver it as quickly as possible
    
    getStaticPaths
    ---------------
        * If a page has Dynamic Routes and uses getStaticProps, it needs to define a list of paths to be statically generated.
        * When you export a function called getStaticPaths (Static Site Generation) from a page that uses dynamic routes, Next.js will statically pre-render all the paths specified by getStaticPaths.
*/

export const getStaticPaths = async () => {

    // sanity language is similar to graph ql
    const query = `*[_type == "product"] {
      slug {
        current
      }
    }`; // give me all the products, but return which have current slug property

    const products = await client.fetch(query);

    const paths = products.map((product) => ({
      params: { 
        slug: product.slug.current
      }
    }));
  
    return {
      paths,
      fallback: 'blocking'
    }
}
    
/*
Get static props also have a benefit:
    - can give params
    - Outside of the params can get access actual URL query
        { params: { slug }}
    - slug can be 
        headphones
        speakers

    - From the slug variable value, can query to get the current product details
*/
export const getStaticProps = async ({ params: { slug }}) => {
    // current slug query
    const query = `*[_type == "product" && slug.current == '${slug}'][0]`;

    // similar products query
    const productsQuery = '*[_type == "product"]'
    
    const product = await client.fetch(query);
    const products = await client.fetch(productsQuery);
  
    console.log(product);
  
    return {
      props: { products, product }
    }
  }

export default ProductDetails
